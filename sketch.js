
function setup() {
	createCanvas(windowWidth - 100, windowHeight - 100);
}

function draw() {
	background(210);
	var sinWave = drawSinWave();
	var pulseWave = drawPulseWave();
	var sawWave = drawSawWave();
	var triWave = drawTriWave();
	var noiseWave = drawNoiseWave();
	
	var chosenWaveform = changeWaveform();
	
	fill(0);
	textSize(16);
	text(mouseXY(), 10, 30);
	
	console.log(chosenWaveform);

	if (chosenWaveform === "sinus")
	{
		for (var i = 0; i < sinWave.sinWaveValues.length; i++){
			drawEllipse(0, sinWave.sinWaveValues[i], sinWave.diameter);
		}
	} else if (chosenWaveform === "pulse"){
		for (var i = 0; i < pulseWave.pulseWaveValues.length; i++){
			drawEllipse(0, pulseWave.pulseWaveValues[i], pulseWave.diameter);
		}
	} else if (chosenWaveform === "saw"){
		for (var i = 0; i < sawWave.sawWaveValues.length; i++){
			drawEllipse(0, sawWave.sawWaveValues[i], sawWave.diameter);
		}
	} else if (chosenWaveform === "tri"){
		for (var i = 0; i < triWave.triWaveValues.length; i++){
			drawEllipse(0, triWave.triWaveValues[i], triWave.diameter);
		}
	} else if (chosenWaveform === "noise"){
		for (var i = 0; i < noiseWave.noiseWaveValues.length; i++){
			drawEllipse(0, noiseWave.noiseWaveValues[i], noiseWave.diameter);
		}
	}

}
function drawEllipse(x, y, diameter) {
	ellipse(x, y, diameter, diameter);		
	noStroke();

	translate(diameter + 3, 0);
}
function mouseXY(){
	var x, y;
	var midWidth = width / 2;
	x = (midWidth - mouseX) / 2;

	var midHeight = height / 2;
	y = (midHeight - mouseY) / 2;

	var returnArr = [x,y];
	return returnArr;
}
function drawSinWave(){
	var mid = height / 2;
	var XY = mouseXY();
	var sinWaveValues = [];
	var diameter = 10;	
	var maxLength = (width / diameter) + 1;

	var objReturn = {
		sinWaveValues: sinWaveValues,
		diameter: diameter
	};

	for (var i = 0; i < maxLength; i++){
		sinWaveValues.push(mid + sin(i/2 + millis()/XY[0]) * XY[1]);
	}

	return objReturn;
}
function drawPulseWave(){
	var mid = height / 2;
	var XY = mouseXY();
	var pulseWaveValues = [];
	var diameter = 10;	
	var maxLength = (width / diameter) + 1;
	
	var objReturn = {
		pulseWaveValues: pulseWaveValues,
		diameter: diameter
	};
	

	for (var i = 0; i < maxLength; i++) {
		if (sin(i/2 + millis()/64) <= 60) {
			pulseWaveValues.push(mid - (XY[1]));
		} else {
			pulseWaveValues.push(mid + (XY[1]));
		}			
	}
	return objReturn;
}

function drawSawWave(){
	var mid = height / 2;
	var XY = mouseXY();
	var sawWaveValues = [];
	var diameter = 10;	
	var maxLength = (width / diameter) + 1;

	var objReturn = {
		sawWaveValues: sawWaveValues,
		diameter: diameter
	};
	
	// for (var i = 0; i < maxLength; i++){
		// sawWaveValues.push(mid + (-(2*XY[1]/PI)*atan(1/tan((i + (millis()/XY[0])*PI)/10))));
	// }
	
	for (var i = 0; i < maxLength; i++){
		sawWaveValues.push(mid + (-(2*XY[1]/PI)*atan(1/tan((i + (millis()/100)*PI)/(XY[0]/10)))));
	}	
	// console.log(sawWaveValues);
	return objReturn;

}

function drawTriWave(){
	var mid = height / 2;
	var XY = mouseXY();
	var triWaveValues = [];
	var diameter = 10;	
	var maxLength = (width / diameter) + 1;
	
	var p = (8);
	
	for (var i = 0; i < maxLength; i++){
		triWaveValues.push(mid + (((2*XY[1])/PI))*asin(sin(((2*PI)/(p+XY[0]))*i/2 + millis()/200)));
	}	
	var objReturn = {
		triWaveValues: triWaveValues,
		diameter: diameter
	};
	return objReturn;
}

function drawNoiseWave(){
	var mid = height / 2;
	var XY = mouseXY();
	var noiseWaveValues = [];
	var diameter = 10;	
	var maxLength = (width / diameter) + 1;	
		
	for (var i = 0; i < maxLength; i++){
		// noiseWaveValues.push(mid + ((noise(i+(millis()/XY[0])*2))*(XY[1])));
		noiseWaveValues.push(mid + ((random(-1,1))*XY[1]));
	}
	
	var objReturn = {
		noiseWaveValues: noiseWaveValues,
		diameter: diameter
	};
			
	return objReturn;
}

function changeWaveform(){
	var test = document.getElementById("b");
	// console.log(test);
	// console.log(test.options[test.selectedIndex].value);
	
	return (test.options[test.selectedIndex].value);
	// if (test.options[test.selectedIndex] == "sinus")
	// {
		// for (var i = 0; i < sinWave.sinWaveValues.length; i++){
			// drawEllipse(0, sinWave.sinWaveValues[i], sinWave.diameter);
		// }
	// }
}